require('dotenv/config');
const Telegraf = require('telegraf');
const constants = require('./utils/constants');
const { events } = require('./events');
const { commands } = require('./commands');

const { session } = Telegraf;

let URL = '';
let PORT = 4000;
let BOT_TOKEN = '';

function loadConstants() {
  URL = process.env.URL;
  PORT = process.env.PORT || 4000;
  BOT_TOKEN = process.env.BOT_TOKEN || '';
}

function catchErrors(err, ctx) {
  console.log(`Error: ${ctx.updateType}`, err);
}

const getConstant = name => constants.texts()[name];

function basicCommands(bot) {
  bot.start(ctx => {
    ctx.reply(getConstant('WELLCOME_INITIAL_MESSAGE'));
    ctx.reply(getConstant('WELLCOME_INITIAL_MESSAGE_2'));
  });
  bot.help(ctx => ctx.reply(getConstant('COMMAND_LIST')));
  bot.catch((err, ctx) => catchErrors(err, ctx));
}

function loadEvents(bot) {
  bot.on('new_chat_members', events.newMembers);
  bot.on('sticker', events.sticker);
}

function setCommands(bot) {
  bot.command(ctx => commands.polls(ctx));
}

const init = async bot => {
  bot.use(session());
  basicCommands(bot);
  loadEvents(bot);
  setCommands(bot);
  // bot.hears(/[hola|ola]/g, ctx => ctx.reply('Hey there'));
  return bot;
};

exports.init = () => {
  loadConstants();
  init(new Telegraf(BOT_TOKEN, {})).then(bot => {
    console.log('Setting Webhook:');
    console.log(`${URL}/bot${BOT_TOKEN}`);
    bot.telegram.setWebhook(`${URL}/bot${BOT_TOKEN}`, null, PORT);
    console.log('Starting Webhook');
    bot.startWebhook(`/bot${BOT_TOKEN}`, null, PORT);
    console.log('Webhook started');
  });
};
