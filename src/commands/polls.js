const { typePolls } = require('../utils/constants');
const { createPoll } = require('../utils/poll');

exports.polls = ctx => {
  createPoll(ctx, typePolls[ctx.message.text.slice(1)]);
};
