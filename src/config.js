require('dotenv/config');
const spawn = require('await-spawn');

const initialize = async () => {
  const mode = process.env.MODE;
  if (mode === 'DEVELOPMENT') {
    const url = await spawn('./url.sh', { shell: true });
    process.env.URL = url.toString().replace(/(\r\n|\n|\r)/gm, '');
  }
};

exports.config = {
  initialize,
};
