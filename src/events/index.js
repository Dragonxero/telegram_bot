const { newMembers } = require('./new_members');
const { sticker } = require('./sticker');

exports.events = { newMembers, sticker };
