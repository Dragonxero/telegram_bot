const constants = require('../utils/constants');

const getConstant = name => constants.constants()[name];

exports.newMembers = async (ctx, next) => {
  ctx.reply(`${getConstant('WELLCOME_NEW_USER')} ${ctx.message.new_chat_member.username}`);
  return next();
};
