require('dotenv/config');
const Telegram = require('telegraf/telegram');
const bender = require('../stickerList/benderList');

const telegram = new Telegram(process.env.BOT_TOKEN);

function benderSticker() {
  return Math.floor(Math.random() * bender.length);
}

function sendSticker(ctx, next) {
  ctx.reply(telegram.sendSticker(ctx.chat.id, bender[benderSticker()].file_id));
  next();
}

exports.sticker = async (ctx, next) => sendSticker(ctx, next);
