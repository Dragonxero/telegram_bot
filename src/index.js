const { config } = require('./config');
const bot = require('./bot');

config.initialize().then(() => {
  bot.init();
});
