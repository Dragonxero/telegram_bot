const bender = [
  {
    emoji: '😂',
    file_id: 'CAACAgIAAxUAAV4vJb87ZDdANk_KOJ2MUqr1x7xXAAIUAwACz7vUDuUkNw-Ey1cWGAQ',
  },
  {
    emoji: '😘',
    file_id: 'CAACAgIAAxUAAV4vJb8Y-Bs14fGmLIA9WF3l40GJAAIVAwACz7vUDmfSj1Fqgb-6GAQ',
  },
  {
    emoji: '😁',
    file_id: 'CAACAgIAAxkBAAObXi8nUCmLCS7nTgFHAAHCHn_YO94mAAIkAwACz7vUDm0PI10rjwfcGAQ',
  },
  {
    emoji: '😨',
    file_id: 'CAACAgIAAxUAAV4vJb_-5JZgUTIx8VKM4f95W8qiAAIWAwACz7vUDuUef92K6MHnGAQ',
  },
  {
    emoji: '😁',
    file_id: 'CAACAgIAAxUAAV4vJb9lhZipyw7Juix20oIDNKPCAAIqAwACz7vUDh_ytRh_ZZ8wGAQ',
  },
  {
    emoji: '😠',
    file_id: 'CAACAgIAAxUAAV4vJb9p3OkTYaH3kZgrJj2mpMyAAAIXAwACz7vUDn1p2xCESM-tGAQ',
  },
  {
    emoji: '😁',
    file_id: 'CAACAgIAAxUAAV4vJb_NoBQBR9G7UoAdUTmDRvZ_AAIxAwACz7vUDqW2vCpJP1jKGAQ',
  },
  {
    emoji: '🕺',
    file_id: 'CAACAgIAAxUAAV4vJb-QiGJ3kwcZKFDh89gzxAABJQACGAMAAs-71A5auZHETBwZHRgE',
  },
  {
    emoji: '😤',
    file_id: 'CAACAgIAAxkBAAOlXi8oWv9NFW6km5VYdpjWmZmogaEAAjIDAALPu9QO7NG_rwHLEAMYBA',
  },
  {
    emoji: '😨',
    file_id: 'CAACAgIAAxUAAV4vJb_vTBLT6XpPW3HY_UjdlB1hAAIzAwACz7vUDiKkHsDpWirrGAQ',
  },
  {
    emoji: '😲',
    file_id: 'CAACAgIAAxUAAV4vJb-fAsT3O93aeXWqBBAeEQ6DAAIfAwACz7vUDh3I-WMxrv5vGAQ',
  },
  {
    emoji: '😴',
    file_id: 'CAACAgIAAxUAAV4vJb_rmnG5uHieJBpW60UE0OwbAAIZAwACz7vUDmy0QE4beBS9GAQ',
  },
  {
    emoji: '😊',
    file_id: 'CAACAgIAAxUAAV4vJb9QwHjHokMNLRRcwcmVQm72AAItAwACz7vUDvjrJ74jhFYAARgE',
  },
  {
    emoji: '🧚‍♀️',
    file_id: 'CAACAgIAAxUAAV4vJb_qxX7y_B0QCk3mUlvp7TUsAAIhAwACz7vUDsQ2zDWh8diCGAQ',
  },
  {
    emoji: '😡',
    file_id: 'CAACAgIAAxUAAV4vJb-mUQF-IhWnNS9M32pAPODyAAIeAwACz7vUDg7n4pOOVJEUGAQ',
  },
  {
    emoji: '😐',
    file_id: 'CAACAgIAAxUAAV4vJb9oFtJyeEIWSbGO9DQEdkeAAAIbAwACz7vUDsIc3bMyqex1GAQ',
  },
  {
    emoji: '🍻',
    file_id: 'CAACAgIAAxUAAV4vJb9bCW5y2fs2O3cMUKbwbq_kAAIcAwACz7vUDq5GJ41repEoGAQ',
  },
  {
    emoji: '😢',
    file_id: 'CAACAgIAAxkBAAORXi8mx3zNuVrGoWbFZhfjk4-bIzkAAiADAALPu9QOI4o4PMILtaAYBA',
  },
  {
    emoji: '🥴',
    file_id: 'CAACAgIAAxkBAAOfXi8nrvRONNTG5eLvJdrfdTo5XAYAAhoDAALPu9QOCnEbFVbqXAMYBA',
  },
  {
    emoji: '💏',
    file_id: 'CAACAgIAAxUAAV4vJb-LQ2Eu29S9LbtrWe8P9Be5AAIiAwACz7vUDil7zc0S0dAYGAQ',
  },
  {
    emoji: '😄',
    file_id: 'CAACAgIAAxUAAV4vJb86p9aos0Lv9t1poiB2e1Y-AAIlAwACz7vUDn72oSUPiLS8GAQ',
  },
  {
    emoji: '👌',
    file_id: 'CAACAgIAAxUAAV4vJb9kNH2wMS0Gls2p6h_ZWkvCAAImAwACz7vUDqRT7fQiGuLvGAQ',
  },
  {
    emoji: '😫',
    file_id: 'CAACAgIAAxUAAV4vJb_VlVEzUuggZLXqFTViih-JAAIuAwACz7vUDs7ljZ7L-0WsGAQ',
  },
  {
    emoji: '😄',
    file_id: 'CAACAgIAAxUAAV4vJb-w-fYVitB50pNF3ZdKbSTXAAIoAwACz7vUDtZMvbygR7NjGAQ',
  },
  {
    emoji: '😈',
    file_id: 'CAACAgIAAxUAAV4vJb9ZcN52DJRxZIE0CVHt988BAAIpAwACz7vUDv88aesy9rheGAQ',
  },
  {
    emoji: '👙',
    file_id: 'CAACAgIAAxUAAV4vJb-wfWZFa7orxo6KHr2kPwlVAAIsAwACz7vUDrne-QLHM80RGAQ',
  },
  {
    emoji: '😁',
    file_id: 'CAACAgIAAxUAAV4vJb_GjvVHOgiP6DY27cD4IaC2AAIrAwACz7vUDoLu1J5tqV6nGAQ',
  },
  {
    emoji: '😁',
    file_id: 'CAACAgIAAxUAAV4vJb8etpcWBZquHMe6JnInswWEAAInAwACz7vUDocQunLkc1mbGAQ',
  },
  {
    emoji: '😁',
    file_id: 'CAACAgIAAxUAAV4vJb-_-7VYmGV6xIeWkFljO5DSAAIvAwACz7vUDpP-XmS_XaOuGAQ',
  },
  {
    emoji: '😎',
    file_id: 'CAACAgIAAxUAAV4vJb-lWsCRibmxDahsRLxbY-RUAAIwAwACz7vUDuGjlyaQSevGGAQ',
  },
];

module.exports = bender;
