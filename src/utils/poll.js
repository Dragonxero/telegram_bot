require('dotenv/config');
const Telegram = require('telegraf/telegram');

const telegram = new Telegram(process.env.BOT_TOKEN);

const Timeout = (ctx, data) =>
  setTimeout(() => {
    ctx.reply('Load response');
    ctx.reply(data.message || 'Apuren');
  }, data.time);

exports.createPoll = (ctx, data) => {
  telegram.sendPoll(ctx.chat.id, data.title || 'Encuesta', data.choices || [], data.options || {});
  if (data.timeout.set) Timeout(ctx, data.timeout);
};
